# FocusKurs
Projekt bazowy utworzony dla kursu org. przez Focus Telecom, główne założenia tej wersji:

* Możliwe ograniczenie używania wersji ES5 Javascript.
* Możliwe ograniczenie wykorzystania biblioteki RxJS.

# Ficzery
* Zarówno backend jak i frontend zawarte są w ramach jednego środowiska współdzieją tym samym wspólnie node_modules.
* Aplikacja odpytuje serwer o status połączenia.
* Możliwość automatycznego wywołania dzwonienia po linku /call/(numer)
* Wykorzystanie routera (maksymalnie uproszczone)

# Uruchomienie
* Backend: node server.js
* Frontend: ng serve


# Angular-Cli
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.5.
